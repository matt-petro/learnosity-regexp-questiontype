<?php

// Authoing UI example for custom regexp question type

include_once 'config.php';
include_once 'src/includes/header.php';

use LearnositySdk\Request\Init;
use LearnositySdk\Utils\Uuid;

$session_id = Uuid::generate();

$security = [
    'user_id'      => $studentid,
    'domain'       => $domain,
    'consumer_key' => $consumer_key
];

$request = '{
  "id": "custom-regexp",
  "type": "local_practice",
  "state": "initial",
  "session_id": "' . $session_id . '",
  "questions": []
}';


$init = new Init('questions', $security, $consumer_secret, $request);
$signedRequest = $init->generate();

?>
<div class="jumbotron section">
    <div class="toolbar">
        <ul class="list-inline">
            <li data-toggle="tooltip" data-original-title="Preview API Initialisation Object"><a href="#"  data-toggle="modal" data-target="#initialisation-preview"><span class="glyphicon glyphicon-search"></span></a></li>
            <li data-toggle="tooltip" data-original-title="Visit the documentation"><a href="http://docs.learnosity.com/assessment/questions/knowledgebase/customquestions" title="Documentation"><span class="glyphicon glyphicon-book"></span></a></li>
            <li data-toggle="tooltip" data-original-title="Toggle product overview box"><a href="#"><span class="glyphicon glyphicon-chevron-up jumbotron-toggle"></span></a></li>
        </ul>
    </div>
    <div class="overview">
        <h1>Custom Question - Regular expression Short Text</h1>
    </div>
</div>

<div class="section">
    <div class="row">
        <div id="lrnQuestionEditor">
        </div>
    </div>
</div>

<script src="<?php echo $url_questioneditor; ?>"></script>
<script src="<?php echo $url_questions; ?>"></script>
<script src="custom_regexp_template.js"></script>


<script>
    (function () {
      var base_url = document.URL.substr(0,document.URL.lastIndexOf('/'));
        var custom_question_url = base_url+'/custom_regexp_q.js';
        var custom_scorer_url = base_url+'/custom_regexp_s.js';
        var custom_editor_layout_url = base_url+'/custom_regexp.html';
        var custom_css_url = base_url+'/custom_regexp.css';

        custom_regexp_type_template.defaults.js.question = custom_question_url;
        custom_regexp_type_template.defaults.js.scorer = custom_scorer_url;
        custom_regexp_type_template.defaults.css = custom_css_url;
        custom_regexp_type.js.question = custom_question_url;
        custom_regexp_type.js.scorer = custom_scorer_url;
        custom_regexp_type.css = custom_css_url;
        custom_regexp_type.editor_layout = custom_editor_layout_url;

        var initOptions = {
            configuration: {
                consumer_key: '<?php echo $consumer_key; ?>'
            },
            question_type_groups: [
                {
                    "reference" : "custom_question",
                    "name" : "Custom question collection",
                    "group_icon": "https://www.learnosity.com/static/img/features/features_proven.png"
                }
            ],
            question_type_templates: {
                custom_regexp_questions_api: [
                    custom_regexp_type_template
                ]
            },
            custom_question_types: [
                custom_regexp_type
            ]
        };

        var getNested = function (object, attributeString) {
            if (object) {
                var split = attributeString.match(/(.+?)\.(.*)/);
                if (split) {
                    return getNested(object[split[1]], split[2]);
                }

                return object[attributeString];
            }
        };
        var listenForQEAttributesChange = function (qeApp, keys, callback) {
            var repository = {};
            var widgetJson = qeApp.getWidget();

            $.each(keys, function (__, key) {
                repository[key] = widgetJson[key];
            });

            qeApp.on('revisionHistoryState:change', function () {
                var widgetJson = qeApp.getWidget();

                if (widgetJson) {
                    $.each(repository, function (key, value) {
                        var newValue = widgetJson[key];

                        if (newValue !== value) {
                            repository[key] = newValue;

                            callback({
                                key: key,
                                value: newValue
                            });
                        }
                    });
                }
            });
        };

        // ===============================================================================
        // Using Questions API to render the current Custom Question as inline element
        // ===============================================================================
        var _count = 0;
        var regexpInlineQuestionsApiRenderer = function ($editPanel, qeApp, questionsApp) {
            this.$editPanel = $editPanel;
            this.qeApp = qeApp;
            this.questionsApp = questionsApp;

            this.init();
            this.render();
        };

        regexpInlineQuestionsApiRenderer.prototype.init = function () {
            listenForQEAttributesChange(this.qeApp, ['max_lines'], function () {
                this.render();
            }.bind(this));

            this.$editPanel
                .find('[data-custom-regexp-element]')
                .replaceWith('<div class="questionContainer"></div>');

            this.$questionContainer = this.$editPanel.find('.questionContainer');
        };

        regexpInlineQuestionsApiRenderer.prototype.render = function () {
            var widgetJson = this.qeApp.getWidget();
            var value = getNested(widgetJson, 'valid_response');
            var responseId = 'inline-' + _count;
            var responses = {};

            // NOTE that in this case we use `responses` argument because we want the new question instance
            // to be "resumed" with validation.valid_response.value as its value. If you are not using current
            // question instance to update
            responses[responseId] = {
                value: value
            };

            this.$questionContainer
                .html('<span class="learnosity-response question-' + responseId + '"/>');

            this.questionsApp.append({
                questions: [
                    $.extend(widgetJson, {
                        response_id: responseId
                    })
                ],
                responses: responses
            });

            this._responseId = responseId;

            _count++;
        };

        regexpInlineQuestionsApiRenderer.prototype.appendInstanceComplete = function () {
            var _instance = this.questionsApp.question(this._responseId);

            if (_instance) {
                _instance.on('change', function () {
                    var newValue = _instance.getResponse().value;
                    var validationValueAttr = this.qeApp.attribute('valid_response');

                    validationValueAttr.setValue(newValue);
                }.bind(this));
            }
        };

        regexpInlineQuestionsApiRenderer.prototype.getQuestionInstance = function () {
            var questionsApp = this.questionsApp;

            return new Promise(function (resolve, reject) {
                setInterval(function () {
                    this.questionsApp.question(responseId);
                }, 10);
            });
        };

        regexpInlineQuestionsApiRenderer.prototype.reset = function () {
            this.qeApp.off('revisionHistoryState:change');
        };
        // ===============================================================================
        var customQuestionHandlers = {
            custom_regexp_inline_questions_api: regexpInlineQuestionsApiRenderer
        };
        var questionEditorApp = LearnosityQuestionEditor.init(initOptions, '#lrnQuestionEditor');
        var _hiddenQuestionApp = LearnosityApp.init(<?php echo $signedRequest; ?>, {
            readyListener: function () {
                // For inline question renderer to detect when a new question instance has been appended successfully,
                // we need to rely on this readyListener to tell when an instance is ready
                if (_handler && _handler.appendInstanceComplete) {
                    _handler.appendInstanceComplete();
                }
            }
        });
        var _handler, _activeInlineQuestionInstance;

        // Listen to the right events to bootstrap our custom handler
        questionEditorApp.on('widget:ready', function (data) {
            var widget = questionEditorApp.getWidget();
            var Handler;

            if (widget.type === 'custom') {
                Handler = customQuestionHandlers[widget.custom_type];

                if (Handler) {
                    _handler = new Handler($(data.wrapper), questionEditorApp, _hiddenQuestionApp);
                }
            }
        });

        questionEditorApp.on('widget:changed', function () {
            if (_handler) {
                _handler.reset && _handler.reset();
                _handler = null;
            }
        });

        // Expose window context in case we want to access the public method of questionEditorApp
        window.questionEditorApp = questionEditorApp;
    })();
</script>
<?php
include_once 'includes/footer.php';

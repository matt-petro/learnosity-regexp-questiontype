/*global LearnosityAmd*/
LearnosityAmd.define([
    'underscore-v1.5.2',
    'jquery-v1.10.2'
], function (
    _,
    $
) {
    'use strict';

    function getValidResponse(question) {
        return (
            _.isObject(question) &&
            question.validation.valid_response
        ) || null;
    }

    function CustomRegexp(init, lrnUtils) {
        this.init = init;
        this.lrnUtils = lrnUtils;
        this.question = init.question;
        this.$el = init.$el;

        this.setup();

        init.events.trigger('ready');
    }

    _.extend(CustomRegexp.prototype, {
        render: function () {
            var self = this;
            var newelt;
            var multiline = this.question.input_style === 'multiline';
            var max_length = this.question.max_length || 50;
            var max_lines = this.question.max_lines || 5;
            var placeholder = this.question.placeholder? 'placeholder="'+this.question.placeholder+'"' : '';
            
            if (multiline) {
                newelt ='<textarea spellcheck="false" '+placeholder+' />';
            } else {
                newelt ='<input type="text" spellcheck="false" '+placeholder+' maxlength="'+max_length+'" />';
            }

            this.$el
                .html('<div class="custom-regexp"><div class="input-wrapper">'+newelt+'</div></div>')
                .append('<div data-lrn-component="suggestedAnswersList"/>')
                // Add LRN Check Answer button.
                .append('<div data-lrn-component="checkAnswer"/>');
            
            this.lrnUtils.renderComponent('CheckAnswerButton', this.$el.find('[data-lrn-component="checkAnswer"]').get(0));
            
            if (multiline) {
                this.$el
                    .find('textarea')
                    .attr('rows', max_lines)
                    .keydown(function(e) { 
                        // enforce max_length while typing
                        if (e.key.length === 1) { // if printable character
                            var val = $(this).val();
                            if (val.length >= max_length) {
                                // Flash a warning and block the event.
                                var target = $(this);
                                target.addClass('lrn_length_warning')
                                setTimeout(function () { 
                                    target.removeClass('lrn_length_warning');
                                }, 200);
                                
                                return false;
                            }
                        }
                        return true;
                    })
                    .change(function(e) {
                        // enforce max_length with other changes (paste, etc)
                        $(this).val( $(this).val().substr(0,self.question.max_length));
                    });
            }
        },

        setup: function () {
            var init = this.init;
            var events = init.events;
            var facade = init.getFacade();

            this.updatePublicMethods(facade);
            this.render();

            this.$response = $('textarea,input', this.$el);
            this.$correctAnswers = $('.lrn_correctAnswers', this.$el);

            if (init.response) {
                this.$response.val(init.response);
            }

            this.$response
                .on('focus', function () {
                    this.clearValidationUI();
                    this.hideCorrectAnswers();
                }.bind(this))
                .on('change keyup paste', function (event) {
                    events.trigger('changed', event.currentTarget.value);
                });

            events.on('validate', function (options) {
                var result = facade.isValid(); // Use facade.isValid(true) to get the detailed report

                this.clearValidationUI();
                this.showValidationUI(result);

                if (!result && options.showCorrectAnswers) {
                    this.showCorrectAnswers();
                }
            }.bind(this));
        },

        showValidationUI: function (isCorrect) {
            this.$el
                .find('.input-wrapper')
                // Add this class to display default Learnosity correct, incorrect style
                .addClass(isCorrect ? 'lrn_correct' : 'lrn_incorrect')
                // Add this element if you want to display to corresponding validation (cross, tick) icon
                .append('<span class="lrn_validation_icon"/>');
        },

        clearValidationUI: function () {
            this.$correctAnswers
                .addClass('lrn_hide')
                .find('.lrn_correctAnswerList')
                .empty();

            var $validatedResponse = this.$el
                .find('.input-wrapper')
                .removeClass('lrn_incorrect lrn_correct');

            $validatedResponse.find('.lrn_validation_icon').remove();
            $validatedResponse.find('.lrn_responseIndex').remove();
        },

        showCorrectAnswers: function () {
            var self = this;
            var answer = getValidResponse(this.question);
            var correct_value = this.question.correct_answer;
            correct_value = (correct_value === undefined)? '' : correct_value;

            if (correct_value === '') {
                // No correct answer, use the correct answer if it isn't a regexp.  
                if (answer && !answer.regexp) {
                    correct_value = answer.value;
                }
            }
            if (correct_value !== '') {
                var setAnswersToSuggestedList = function () {
                    // Pass in string to display correct answer list without the index
                    self.suggestedAnswersList.setAnswers(correct_value);
                };

                if (!this.suggestedAnswersList) {
                    this.lrnUtils.renderComponent('SuggestedAnswersList', this.$el.find('[data-lrn-component="suggestedAnswersList"]').get(0))
                        .then(function (component) {
                            self.suggestedAnswersList = component;

                            setAnswersToSuggestedList();
                        });
                } else {
                    setAnswersToSuggestedList();
                }
            }
        },

        hideCorrectAnswers: function () {
            if (this.suggestedAnswersList) {
                // Clear current suggsted answer list
                this.suggestedAnswersList.reset();
            }
        },

        updatePublicMethods: function (facade) {
            var self = this;

            // Override mandatory public methods
            var _enable = facade.enable;
            facade.enable = function () {
                _enable();
                self.$response.prop('disabled', false);
            };

            var _disable = facade.disable;
            facade.disable = function () {
                _disable();
                self.$response.prop('disabled', true);
            };

            // Add new public methods
            facade.reset = function () {
                self.$response
                    .val('')
                    .trigger('changed');
            };
        }
    });

    return {
        Question: CustomRegexp
    };
});

/*global LearnosityAmd*/
LearnosityAmd.define(['underscore-v1.5.2'], function (_) {
    'use strict';

    function trimWhitespace(text, ignoreChars) {
        if (text === undefined) {
            text = ''
        }
        // Convert question.ignore_chars to whitespace
        if (ignoreChars) {
            for (var i = 0; i < ignoreChars.length; i++) {
                text = text.split(ignoreChars[i]).join(' ');
            }
        }
        // Remove leading/trailing whitespace, including empty lines
        text = text.trim();
        // Remove extra internal whitespace, preserving newlines
        text = text.replace(/\t/g, ' ');
        text = text.replace(/^ +| +$/gm, '');
        text = text.replace(/ {2,}/g, ' ');
        return text;
    }

    function regexpEscape(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }

    function checkAnswer(answer, response, question) {
        var answer_value, flags, pattern, re;
        response = trimWhitespace(response, question.ignore_chars);
        if (answer.regexp) {
            // For regexp, do full whitespace trimming on answer, but don't remove ignore_chars
            answer_value = trimWhitespace(answer.value, '', true);
        } else {
            // For non-regexp, do full whitespace trimming on answer and escape
            answer_value = regexpEscape(trimWhitespace(answer.value, question.ignore_chars, true));
        }
        flags = question.case_sensitive? '' : 'i';

        if (answer.matching_rule === 'contains') {
            // Anchor to word boundaries
            pattern = '\\b(?:'+answer_value+')\\b';
        } else {
            // Anchor to the start and end of the string, unless the answer already contains an anchor
            if (answer_value[0] === '^' || answer_value[answer_value.length-1] === '$') {
                pattern = answer_value;
            } else {
                pattern = '^(?:'+answer_value+')$';
            }
        }

        try {
            re = new RegExp(pattern, flags);
        }
        catch(e) {
            return false;
        }
        return re.test(response);
    }
        
    function scoreResponse(response, question) {
        if (!question.validation.valid_response) {
            return null;
        }
        var answers = [question.validation.valid_response]
        if (_.isObject(question.validation.alt_responses)) {
            for (var i = 0; i < question.validation.alt_responses.length; ++i) {
                answers.push(question.validation.alt_responses[i]);
            }
        }
        var score = -1;
        var bestAnswer;
        // Answer '*' or '.*' is treated as a special case and matches after
        // all other answers.  This can be used to assign a special score and feedback
        // to responses that don't match any known pattern so that they can be graded
        // manually.
        var starAnswer;
        for (var i = 0; i < answers.length; ++i) {
            var answer = answers[i];
            if (answer.value === '.*' || answer.value === '*') {
                starAnswer = answer;
            } else {
                if (checkAnswer(answer, response, question)) {
                    if (answer.score > score) {
                        score = answer.score;
                        bestAnswer = answer;
                    }
                }
            }
        }
        return bestAnswer? bestAnswer : starAnswer;
    }

    function getMaxScore(question) {
        if (question.validation.automarkable === false) {
            return question.validation.max_score;
        }
        var maxscore = question.validation.valid_response.score || 0
        if (_.isObject(question.validation.alt_responses)) {
            for (var i = 0; i < question.validation.alt_responses.length; ++i) {
                if (question.validation.alt_responses[i].score > maxscore) {
                    maxscore = question.validation.alt_responses[i].score;
                }
            }
        }
        if ((question.validation.min_score_if_attempted || 0) > maxscore) {
            maxscore = question.validation.min_score_if_attempted;
        }
        return maxscore;
    }

    function CustomRegexpScorer(question, response) {
        this.question = question;
        this.response = response;
        this.matchingAnswer = scoreResponse(response, question);
    }

    _.extend(CustomRegexpScorer.prototype, {

        isValid: function () {
            // valid if at least one answer matched, regardless of score
            return !!this.matchingAnswer;
        },

        score: function () {
            var min_score = this.question.validation.min_score_if_attempted || 0;
            if (this.response === undefined || this.response.trim() === '') {
                return 0;
            }
            if (this.isValid()) {
                return (this.matchingAnswer.score > min_score)? this.matchingAnswer.score : min_score;
            } else {
                return min_score;
            }
        },

        maxScore: function () {
            return getMaxScore(this.question);
        },
        
        canValidateResponse: function () {
            return !!this.question.validation.valid_response;
        },

        // Custom function to return per-response feedback.
        // AJ can use this to show feedback on a per-answer basis.
        responseFeedback: function () {
            return this.matchingAnswer? this.matchingAnswer.feedback : null;
        }
    });

    return {
        Scorer:   CustomRegexpScorer
    };
});


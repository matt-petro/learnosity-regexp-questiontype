// 
// Custom Regexp authoring UI templates
//  
//  This probably should be a module.
//

    var custom_regexp_editorschema = {
        "hidden_question": false,
        "properties": {
            "validation": {
                "name": "Set correct answer(s)",
                "attributes": {
                    "allow_negative_scores": {
                        "default": false,
                        "description": "Negative scores will be normalised to zero by default. Allowing negative scores, on the other hand, means that the score can drop below zero when penalties are applied.",
                        "name": "Allow negative scores",
                        "required": false,
                        "type": "boolean"
                    },
                    "penalty": {
                        "default": 0,
                        "min": 0,
                        "required": false,
                        "type": "number",
                        "name": "Penalty point(s)",
                        "description": "A value which indicates how many marks will be deducted for an incorrect response."
                    },
                    "min_score_if_attempted": {
                        "default": 0,
                        "description": "Positive value indicating the minimum score if a student attempted the question.",
                        "min": 0,
                        "name": "Minimum score if attempted",
                        "required": false,
                        "type": "number",
                        "reset_and_disable": {
                            "dependency": "validation.unscored",
                            "dependency_value": true
                        }
                    },
                    "scoring_type": {
                        "default": "exactMatch",
                        "element": "select",
                        "name": "Scoring type",
                        "description": "The way in which marks are distributed for the question. <ul><li><strong>Exact Match</strong> - All parts of the question must be answered correctly to receive a mark.</li></ul>",
                        "options": [
                            {
                                "label": "Exact match",
                                "value": "exactMatch"
                            }
                        ],
                        "required": false,
                        "type": "string"
                    },
                    "unscored": {
                        "type": "boolean",
                        "required": false,
                        "name": "Unscored/Practice usage",
                        "default": false,
                        "description": "When enabled, this option will remove all scoring from the question. This is useful for creating practice questions."
                    },
                    "valid_response": {
                        "attributes": {
                            "score": {
                                "default": 1,
                                "min": 0,
                                "required": true,
                                "type": "number",
                                "name": "Point(s)",
                                "description": "Score awarded for the correct response(s).",
                                "reset_and_disable": {
                                    "dependency": "validation.unscored",
                                    "dependency_value": true
                                }
                            },
                            "matching_rule": {
                                "default": "exact",
                                "element": "select",
                                "options": [
                                    {
                                        "label": "Exact match",
                                        "value": "exact"
                                    },
                                    {
                                        "label": "Any text containing",
                                        "value": "contains"
                                    },
                                ],
                                "description": "This is a setting for author to define which rule to be used to evaluate student's input.",
                                "name": "Allow",
                                "required": false,
                                "type": "string"
                            },
                            "regexp": {
                                "description": "Whether to evaluate as a regular expression.",
                                "default": true,
                                "name": "Regular expression",
                                "required": true,
                                "type": "boolean",
                            },
                            "value": {
                                "name": "Value",
                                "required": true,
                                "type": "textarea"
                            },
                            "feedback": {
                                "name": "Feedback",
                                "required": false,
                                "type": "editor"
                            }
                        },
                        "required": true,
                        "type": "object",
                        "name": "Correct",
                        "description": "The correct response for the question."
                    },
                    "automarkable": {
                        "description": "Defines whether the question will be marked automatically, or must be marked manually.",
                        "default": true,
                        "required": false,
                        "type": "boolean",
                        "name": "Enable auto scoring",
                        "hide_sections": {
                            "sections": [
                                "validation"
                            ],
                            "dependency_value": false
                        }
                    },
                    "alt_responses": {
                        "description": "Add an alternate response if there is more than one correct overall solution to a question.",
                        "items": {
                            "attributes": {
                                "score": {
                                    "default": 1,
                                    "description": "",
                                    "min": 0,
                                    "required": true,
                                    "type": "number",
                                    "name": "Point(s)",
                                    "reset_and_disable": {
                                        "dependency": "validation.unscored",
                                        "dependency_value": true
                                    }
                                },
                                "matching_rule": {
                                    "default": "exact",
                                    "element": "select",
                                    "options": [
                                        {
                                            "label": "Exact match",
                                            "value": "exact"
                                        },
                                        {
                                            "label": "Any text containing",
                                            "value": "contains"
                                        },
                                    ],
                                    "description": "This is a setting for author to define which rule to be used to evaluate student's input.",
                                    "name": "Allow",
                                    "required": false,
                                    "type": "string"
                                },
                                "regexp": {
                                    "description": "Whether to evaluate as a regular expression.",
                                    "default": false,
                                    "name": "Regular expression",
                                    "required": false,
                                    "type": "boolean",
                                },
                                "value": {
                                    "name": "Value",
                                    "required": true,
                                    "type": "textarea"
                                },
                                "feedback": {
                                    "name": "Feedback",
                                    "required": false,
                                    "type": "editor"
                                }
                            },
                            "required": false,
                            "type": "object",
                            "name": "Alternate {{index}}"
                        },
                        "name": "Alternate responses",
                        "required": false,
                        "type": "array"
                    }
                },
                "conditional_attributes": [
                    {
                        "attribute_key": "scoring_type",
                        "conditions": [
                            {
                                "attributes": {
                                    "rounding": {
                                        "type": "string",
                                        "default": "floor",
                                        "element": "select",
                                        "options": [
                                            {
                                                "label": "Round down",
                                                "value": "floor"
                                            },
                                            {
                                                "label": "None",
                                                "value": "none"
                                            }
                                        ],
                                        "name": "Rounding",
                                        "description": "Defines whether score rounding should be applied. Options are:<ul><li><strong>None</strong>: Overall question score will be divided between correct responses and rounded to four decimal places, e.g. 2.6666</li><li><strong>Floor</strong>: Overall question score will be divided between correct responses and rounded down to the nearest whole number.</li></ul>Only works with Partial Match scoring."
                                    }
                                },
                                "value": [
                                    "partialMatchV2"
                                ]
                            },
                            {
                                "attributes": {
                                    "rounding": {
                                        "type": "string",
                                        "default": "floor",
                                        "element": "select",
                                        "options": [
                                            {
                                                "label": "Round down",
                                                "value": "floor"
                                            },
                                            {
                                                "label": "None",
                                                "value": "none"
                                            }
                                        ],
                                        "name": "Rounding",
                                        "description": "Defines whether score rounding should be applied. Options are:<ul><li><strong>None</strong>: Overall question score will be divided between correct responses and rounded to four decimal places, e.g. 2.6666</li><li><strong>Floor</strong>: Overall question score will be divided between correct responses and rounded down to the nearest whole number.</li></ul>Only works with Partial Match scoring."
                                    }
                                },
                                "value": [
                                    "partialMatchElementV2"
                                ]
                            }
                        ]
                    },
                    {
                        "attribute_key": "automarkable",
                        "conditions": [
                            {
                                "attributes": {
                                    "max_score": {
                                        "type": "number",
                                        "min": 0,
                                        "name": "Max score",
                                        "description": "The highest score that can be awarded for the response. Needs to be configured by development team."
                                    }
                                },
                                "value": [
                                    false
                                ]
                            }
                        ]
                    }
                ],
                "description": "In this section, configure the correct answer(s) for the question.",
                "required": false,
                "type": "object",
                "group": "validation"
            },
            
            "feedback_attempts": {
                "default": 0,
                "min": 0,
                "required": false,
                "type": "number",
                "group": "validation",
                "name": "Check answer attempts",
                "description": "If Check Answer is enabled, this field determines how many times a user can click on the Check Answer button. 0 means unlimited."
            },
            "instant_feedback": {
                "default": false,
                "required": false,
                "type": "boolean",
                "group": "validation",
                "name": "Check answer button",
                "description": "Enables the Check Answer button underneath the question, which will provide the student with instant feedback on their response(s)."
            },
            "max_length": {
                "default": 50,
                "min": 1,
                "name": "Character limit",
                "required": false,
                "type": "number",
                "group": "basic",
                "description": "Maximum number of characters that can be entered in the text entry area. Maximum 250 characters."
            },
            "max_lines": {
                "default": '',
                "min": 1,
                "name": "Number of input lines",
                "required": false,
                "type": "number",
                "group": "basic",
                "description": "Number of lines that can be entered in the text entry area."
            },
            "placeholder": {
                "name": "Placeholder",
                "required": false,
                "type": "textarea",
                "group": "basic",
                "description": "Placeholder text that can be added into the response entry area, which disappears when user starts typing."
            },
            "case_sensitive": {
                "default": false,
                "name": "Case sensitive",
                "required": false,
                "type": "boolean",
                "group": "validation",
                "description": "Determines whether the student response must match the letter case set by the author in the correct response. E.g. when enabled, only one of Yes, yes, or YES will be accepted"
            },
            "ignore_chars": {
                "name": "Additional characters to convert to whitespace",
                "required": false,
                "type": "string",
                "group": "validation",
                "description":"The given characters will be treated as whitespace during answer checking"
            },
            "correct_answer": {
                "name": "Correct answer",
                "required": false,
                "type": "textarea",
                "group": "validation",
                "description":"The correct answer to show to the student"
            },
            "input_style": {
                "default": "singleline",
                "element": "select",
                "options": [
                    {
                        "label": "Single line",
                        "value": "singleline"
                    },
                    {
                        "label": "Multiple lines",
                        "value": "multiline"
                    }
                ],
                "description": ".",
                "name": "Input style",
                "required": false,
                "type": "string"
            }
        }
    };

    var custom_regexp_type_template = {
        "name": "Regular expression short text",
        "description": "Regular expression short text",
        "group_reference": "custom_question",
        "image": "//assets.learnosity.com/questiontypes/templates/shorttext.png",
        "defaults": {
            "type": "custom",
            "js": {
                 "question": "BASE_URL_PATH/custom_regexp_q.js",
                 "scorer": "BASE_URL_PATH/custom_regexp_s.js"
            },
            "css": "//localhost/pmatch/custom_regexp.css"
        }
    };

    
    var custom_regexp_type = {
        "custom_type": "custom_regexp_questions_api",
        "type": "custom",
        "name": "Regular expression short text",
        "editor_layout": "BASE_URL_PATH/custom_regexp.html",
        "js": {
             "question": "BASE_URL_PATH/custom_regexp_q.js",
             "scorer": "BASE_URL_PATH/custom_regexp_s.js"
        },
        "css": "//localhost/pmatch/custom_regexp.css",
        "version": "v0.1.0",
        "editor_schema": custom_regexp_editorschema 
    };
